﻿#encoding: UTF-8
require 'spec_helper'

feature 'tạo phiếu chi' do
 scenario "có thể tạo 1 phiếu" do
  visit'/'
  click_link 'phiếu chi mới'
  fill_in 'số phiếu',with:'1'
  fill_in 'tên người tạo phiếu',with:'hovaten'
  fill_in 'địa chỉ',with:'noitaophieu'
  fill_in 'lý do nộp',with:'nophocphi'
  fill_in 'số tiền',with:'200000'
  fill_in 'ngày',with:'3'
  fill_in 'tháng',with:'3'
  fill_in 'năm',with:'1993'
  fill_in 'nguồn kinh phí',with:'doan'
  fill_in 'chủ nhiệm khoa',with:'hoten'
  fill_in 'người lập phiếu',with:'hoten'
  fill_in 'người nộp tiền',with:'hoten'
  fill_in 'thủ quỹ',with:'hoten'
  click_button'tạo phiếu'
  expect(page).to have_content('phiếu chi đã tạo')
  
  chit=Chit.where(name: "hovaten").first
  
  expect(page.current_url).to eql(chit_url(chit))
  
  title="hovaten"
  expect(page).to have_title(title)
 end
end